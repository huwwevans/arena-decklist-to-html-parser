<?php

$files = glob('*.{txt}', GLOB_BRACE);
foreach($files as $file) {

    $board = "0";
$path_parts = pathinfo($file);
    $file_name = $path_parts["filename"];
$file_contents = file_get_contents($file);

    $output = "<html>\n";
    $lines = explode("\n", $file_contents);
    foreach ($lines as $line) {
        if (preg_match("/(\d+) (.+?) \((\w+)\)/", $line, $m)) {
            
            if ($board=="0"){
            $board="Deck";
            $output .= "<h1>Deck</h1>\n";    
                
            }
               if (preg_match("/(Plains)|(Island)|(Swamp)|(Mountain)|(Forest)/",$m[2],$q)){
               preg_match("/(\d+) (.+?) \((\w+)\) (\d+)/", $line, $m);
               $output .= "<p>" . $m[1] . "x <a class=\"card\"" . "data-basiccnumber=\".". $m[4] ."\"data-setcode=\"" . $m[3] . "\">" . $m[2] . "</a></p>\n"; 
            }
            else{
            $output .= "<p>" . $m[1] . "x <a class=\"card\" data-setcode=\"" . $m[3] . "\">" . $m[2] . "</a></p>\n";
            }
            
        }
        
        else if (rtrim($line) !== "" and $board !=="Sideboard") {
            
            $output .= "<h1>".rtrim($line)."</h1>\n";
            $board = rtrim($line);
        }
        
        
        else if ($line == "" and $board =="Deck"){
            $board="Sideboard";
            $output .= "<h1>Sideboard</h1>\n";
            
        }
    }
    $output .= "</html>";
    if (file_put_contents($file_name.".html", $output)) {
        echo "File ". $file_name.".html written successfully!\n";
    } else {
        echo "Unable to write to file!";
    }
}
?>
