# Arena Decklist To HTML Parser

This simple PHP script takes all files with a .txt file extention in the current directory.  

So long as they are in the format used by Magic The Gathering arena it will output a .html file in the format used by my blog:  

"huwbrewsmtg.net".  

For Example.

If the input file "decklist.txt" contains:


```
Commander
1 Daxos, Blessed by the Sun (THB) 9

Companion
1 Lurrus of the Dream Den (IKO) 226

Deck
1 Stonecoil Serpent (ELD) 235
1 Healer's Hawk (GRN) 14
22 Plains (IKO) 262
1 Ajani's Pridemate (WAR) 4
1 Soulmender (M20) 37
1 Hunted Witness (GRN) 15
1 Charmed Stray (WAR) 8
1 Beloved Princess (ELD) 7
1 Alseid of Life's Bounty (THB) 1
1 Charming Prince (ELD) 8
1 Daybreak Chaplain (M20) 12
1 Gingerbrute (ELD) 219
1 Impassioned Orator (RNA) 12
1 Hushbringer (ELD) 18
1 Grateful Apparition (WAR) 17
1 Drannith Healer (IKO) 10
1 Haazda Marshal (GRN) 13
1 Sworn Companions (GRN) 27
1 Shadowspear (THB) 236
1 The Birth of Meletis (THB) 5
1 Sentinel's Mark (RNA) 20
1 Battlefield Promotion (WAR) 5
1 Dawn of Hope (GRN) 8
1 Desperate Lunge (WAR) 266
1 Moment of Heroism (M20) 30
1 Rally for the Throne (ELD) 25
1 Triumphant Surge (THB) 41
1 Take Heart (GRN) 28
1 Light of Hope (IKO) 20
1 Golden Egg (ELD) 220
1 Shatter the Sky (THB) 37
1 Citywide Bust (GRN) 4
1 Unbreakable Formation (RNA) 29
1 Ravnica at War (WAR) 28
1 Inspired Charge (M20) 24
1 Faerie Guidemother (ELD) 11
1 Solid Footing (IKO) 31
1 Sentinel's Eyes (THB) 36

Sideboard
1 Lurrus of the Dream Den (IKO) 226

```





output is as follows in a file named "decklist.html"  


```
<html>
<h1>Commander</h1>
<p>1x <a class="card" data-setcode="THB">Daxos, Blessed by the Sun</a></p>
<h1>Companion</h1>
<p>1x <a class="card" data-setcode="IKO">Lurrus of the Dream Den</a></p>
<h1>Deck</h1>
<p>1x <a class="card" data-setcode="ELD">Stonecoil Serpent</a></p>
<p>1x <a class="card" data-setcode="GRN">Healer's Hawk</a></p>
<p>22x <a class="card" data-setcode="IKO">Plains</a></p>
<p>1x <a class="card" data-setcode="WAR">Ajani's Pridemate</a></p>
<p>1x <a class="card" data-setcode="M20">Soulmender</a></p>
<p>1x <a class="card" data-setcode="GRN">Hunted Witness</a></p>
<p>1x <a class="card" data-setcode="WAR">Charmed Stray</a></p>
<p>1x <a class="card" data-setcode="ELD">Beloved Princess</a></p>
<p>1x <a class="card" data-setcode="THB">Alseid of Life's Bounty</a></p>
<p>1x <a class="card" data-setcode="ELD">Charming Prince</a></p>
<p>1x <a class="card" data-setcode="M20">Daybreak Chaplain</a></p>
<p>1x <a class="card" data-setcode="ELD">Gingerbrute</a></p>
<p>1x <a class="card" data-setcode="RNA">Impassioned Orator</a></p>
<p>1x <a class="card" data-setcode="ELD">Hushbringer</a></p>
<p>1x <a class="card" data-setcode="WAR">Grateful Apparition</a></p>
<p>1x <a class="card" data-setcode="IKO">Drannith Healer</a></p>
<p>1x <a class="card" data-setcode="GRN">Haazda Marshal</a></p>
<p>1x <a class="card" data-setcode="GRN">Sworn Companions</a></p>
<p>1x <a class="card" data-setcode="THB">Shadowspear</a></p>
<p>1x <a class="card" data-setcode="THB">The Birth of Meletis</a></p>
<p>1x <a class="card" data-setcode="RNA">Sentinel's Mark</a></p>
<p>1x <a class="card" data-setcode="WAR">Battlefield Promotion</a></p>
<p>1x <a class="card" data-setcode="GRN">Dawn of Hope</a></p>
<p>1x <a class="card" data-setcode="WAR">Desperate Lunge</a></p>
<p>1x <a class="card" data-setcode="M20">Moment of Heroism</a></p>
<p>1x <a class="card" data-setcode="ELD">Rally for the Throne</a></p>
<p>1x <a class="card" data-setcode="THB">Triumphant Surge</a></p>
<p>1x <a class="card" data-setcode="GRN">Take Heart</a></p>
<p>1x <a class="card" data-setcode="IKO">Light of Hope</a></p>
<p>1x <a class="card" data-setcode="ELD">Golden Egg</a></p>
<p>1x <a class="card" data-setcode="THB">Shatter the Sky</a></p>
<p>1x <a class="card" data-setcode="GRN">Citywide Bust</a></p>
<p>1x <a class="card" data-setcode="RNA">Unbreakable Formation</a></p>
<p>1x <a class="card" data-setcode="WAR">Ravnica at War</a></p>
<p>1x <a class="card" data-setcode="M20">Inspired Charge</a></p>
<p>1x <a class="card" data-setcode="ELD">Faerie Guidemother</a></p>
<p>1x <a class="card" data-setcode="IKO">Solid Footing</a></p>
<p>1x <a class="card" data-setcode="THB">Sentinel's Eyes</a></p>
<h1>Sideboard</h1>
<p>1x <a class="card" data-setcode="IKO">Lurrus of the Dream Den</a></p>
</html>
```
Similarly the following decklist2.txt file:
```
4 Akroan Crusader (THS) 111
4 Burning-Tree Emissary (DDS) 55
4 Crowd's Favor (MYS1) 138
4 Firebrand Archer (HOU) 92
4 Gut Shot (MYS1) 117
4 Kiln Fiend (MYS1) 167
4 Lava Dart (JUD) 134
4 Lightning Bolt (MYS1) 141
14 Mountain (IKO) 269
4 Mutagenic Growth (MM2) 149
4 Simian Spirit Guide (PLC) 148
4 Temur Battle Rage (FRF) 116

4 Blazing Volley (IKO) 107
4 Flame Slash (CN2) 157
4 Flaring Pain (JUD)
3 Tormod's Crypt (MYS1) 278
```

Will ouput the following decklist2.html file:

```
<html>
<h1>Deck:</h1>
<p>4x <a class="card" data-setcode="THS">Akroan Crusader</a></p>
<p>4x <a class="card" data-setcode="DDS">Burning-Tree Emissary</a></p>
<p>4x <a class="card" data-setcode="MYS1">Crowd's Favor</a></p>
<p>4x <a class="card" data-setcode="HOU">Firebrand Archer</a></p>
<p>4x <a class="card" data-setcode="MYS1">Gut Shot</a></p>
<p>4x <a class="card" data-setcode="MYS1">Kiln Fiend</a></p>
<p>4x <a class="card" data-setcode="JUD">Lava Dart</a></p>
<p>4x <a class="card" data-setcode="MYS1">Lightning Bolt</a></p>
<p>14x <a class="card"data-basiccnumber=".269"data-setcode="IKO">Mountain</a></p>
<p>4x <a class="card" data-setcode="MM2">Mutagenic Growth</a></p>
<p>4x <a class="card" data-setcode="PLC">Simian Spirit Guide</a></p>
<p>4x <a class="card" data-setcode="FRF">Temur Battle Rage</a></p>
<h1>Sideboard:</h1>
<p>4x <a class="card" data-setcode="IKO">Blazing Volley</a></p>
<p>4x <a class="card" data-setcode="CN2">Flame Slash</a></p>
<p>4x <a class="card" data-setcode="JUD">Flaring Pain</a></p>
<p>3x <a class="card" data-setcode="MYS1">Tormod's Crypt</a></p>
</html>
```








**Usage**

1) Navigate to the folder containing arena_decklist_parser.php 
2) Create a files "mydeck1".txt "mydeck2".txt... and copy in your decklists from arena (or from any other source that can output that format.)
3) Open a terminal and run the following command as a basic user.  

`$ php arena_decklist_parser.php`


**This Code is Licenced by Huw Evans under the GNU Public Licence V3**

[https://www.gnu.org/licenses/gpl-3.0.html](url)

**scryfalldler is Licenced by nachazo under the GNU Public Licence V3**

All Rights other than those granted in the licence are Reserved.

Cards and sets named in the example above are copyright Wizards of the Coast
This program and My Fansite: huwbrewsmtg.net are unaffiliated.  